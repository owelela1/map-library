package com.pixel.library.map

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.Polyline
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MapCabTest {

    companion object {

        // region helper fields --------------------------------------------------------------------

        @Mock
        var mMockGoogleMapSet: GoogleMap? = null

        @Mock
        var mMockCurrentLatLngSet: LatLng? = null

        @Mock
        var mMockPreviousLatLngFromServerSet: LatLng? = null

        @Mock
        var mMockCurrentLatLngFromServerSet: LatLng? = null

        @Mock
        var mMockDestinationMarkerSet: Marker? = null

        @Mock
        var mMockOriginMarkerSet: Marker? = null

        @Mock
        var mMockGreyPolyLineSet: Polyline? = null

        @Mock
        var mMockBlackPolylineSet: Polyline? = null

        @Mock
        var mMockMovingCabMarkerSet: Marker? = null

        // endregion helper fields -----------------------------------------------------------------

        private var SUT: MapCab? = null
    }

    @Before
    fun setup() {
        SUT = MapCab(
            mMockGoogleMapSet!!,
            mMockCurrentLatLngSet!!,
            mMockPreviousLatLngFromServerSet!!,
            mMockCurrentLatLngFromServerSet!!,
            mMockDestinationMarkerSet!!,
            mMockOriginMarkerSet!!,
            mMockGreyPolyLineSet!!,
            mMockBlackPolylineSet!!,
            mMockMovingCabMarkerSet!!
        )
    }

    @Test
    fun mapCabTest() {
    }
}