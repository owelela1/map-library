package com.pixel.library.map

import android.animation.ValueAnimator
import android.view.animation.LinearInterpolator

object AnimationUtils {

    private const val HUNDRED = 100
    private const val TWO_THOUSAND = 2000L
    private const val THREE_THOUSAND = 3000L

    fun polyLineAnimator(): ValueAnimator {
        val valueAnimator = ValueAnimator.ofInt(0, HUNDRED)
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.duration = TWO_THOUSAND
        return valueAnimator
    }

    fun cabAnimator(): ValueAnimator {
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.duration = THREE_THOUSAND
        valueAnimator.interpolator = LinearInterpolator()
        return valueAnimator
    }
}
