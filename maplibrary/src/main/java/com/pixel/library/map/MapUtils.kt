package com.pixel.library.map

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import kotlin.math.abs
import kotlin.math.atan

@SuppressLint("NoWildcardImports", "WildcardImport")
object MapUtils {

    private const val TAG = "MapUtils"
    private const val TWENTY = 20
    private const val FIFTY = 50
    private const val NINETY = 90
    private const val HUNDRED = 100
    private const val HUNDRED_EIGHTY = 180
    private const val TWO_HUNDRED_SEVENTY = 270

    fun getCarBitmap(context: Context): Bitmap {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_car)
        return Bitmap.createScaledBitmap(bitmap, FIFTY, HUNDRED, false)
    }

    fun getDestinationBitmap(): Bitmap {
        val height = TWENTY
        val width = TWENTY
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), paint)
        return bitmap
    }

    fun getRotation(start: LatLng, end: LatLng): Float {
        val latDifference: Double = abs(start.latitude - end.latitude)
        val lngDifference: Double = abs(start.longitude - end.longitude)
        var rotation = -1F
        when {
            start.latitude < end.latitude && start.longitude < end.longitude -> {
                rotation = Math.toDegrees(atan(lngDifference / latDifference)).toFloat()
            }
            start.latitude >= end.latitude && start.longitude < end.longitude -> {
                rotation =
                    (NINETY - Math.toDegrees(atan(lngDifference / latDifference)) + NINETY).toFloat()
            }
            start.latitude >= end.latitude && start.longitude >= end.longitude -> {
                rotation =
                    (Math.toDegrees(atan(lngDifference / latDifference)) + HUNDRED_EIGHTY).toFloat()
            }
            start.latitude < end.latitude && start.longitude >= end.longitude -> {
                rotation =
                    (NINETY - Math.toDegrees(atan(lngDifference / latDifference)) + TWO_HUNDRED_SEVENTY).toFloat()
            }
        }
        Log.d(TAG, "getRotation: $rotation")
        return rotation
    }
}
