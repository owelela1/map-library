package com.pixel.library.map

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import kotlin.math.roundToInt

@Suppress("LongParameterList", "LongMethod", "FinalNewline", "NoWildcardImports", "WildcardImport")
class MapCab {

    var googleMap: GoogleMap? = null
    var currentLatLng: LatLng? = null
    var previousLatLngFromServer: LatLng? = null
    var currentLatLngFromServer: LatLng? = null
    var destinationMarker: Marker? = null
    var originMarker: Marker? = null
    var greyPolyLine: Polyline? = null
    var blackPolyline: Polyline? = null
    var movingCabMarker: Marker? = null

    companion object {
        private const val ZOOM: Float = 15.5f
        private const val CAM_PADDING: Int = 2
        private const val CAM_WIDTH: Float = 5f
        private const val CAM_ANCHOR: Float = 0.5f
        private const val CAM_INDEX: Float = 100.0f
    }

    constructor(
        googleMapSet: GoogleMap,
        currentLatLngSet: LatLng,
        previousLatLngFromServerSet: LatLng,
        currentLatLngFromServerSet: LatLng,
        destinationMarkerSet: Marker,
        originMarkerSet: Marker,
        greyPolyLineSet: Polyline,
        blackPolylineSet: Polyline,
        movingCabMarkerSet: Marker
    ) {

        googleMap = googleMapSet
        currentLatLng = currentLatLngSet
        previousLatLngFromServer = previousLatLngFromServerSet
        currentLatLngFromServer = currentLatLngFromServerSet
        destinationMarker = destinationMarkerSet
        originMarker = originMarkerSet
        greyPolyLine = greyPolyLineSet
        blackPolyline = blackPolylineSet
        movingCabMarker = movingCabMarkerSet
    }

    fun enableMyLocationOnMap(
        context: Context,
        paddingLeft: Int,
        paddingTop: Float,
        paddingRight: Int,
        paddingBottom: Int
    ): Boolean {
        googleMap?.setPadding(paddingLeft, dpToPx(paddingTop), paddingRight, paddingBottom)
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }
        googleMap?.isMyLocationEnabled = true
        return true
    }

    fun dpToPx(dp: Float): Int {
        val density = Resources.getSystem().displayMetrics.density
        return (dp * density).roundToInt()
    }

    fun moveCamera(latLng: LatLng?) {
        googleMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    fun animateCamera(latLng: LatLng?, zoomSet: Float?) {
        val cameraPosition: CameraPosition = if (zoomSet != null) {
            CameraPosition.Builder().target(latLng).zoom(zoomSet).build()
        } else {
            CameraPosition.Builder().target(latLng).zoom(ZOOM).build()
        }
        googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    fun addCarMarkerAndGet(context: Context, latLng: LatLng): Marker? {
        val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(MapUtils.getCarBitmap(context))
        return googleMap?.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )
    }

    fun addOriginDestinationMarkerAndGet(latLng: LatLng): Marker? {
        val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(MapUtils.getDestinationBitmap())
        return googleMap?.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )
    }

    fun showPath(
        latLngList: List<LatLng>,
        padding: Int?,
        width: Float?,
        anchorU: Float?,
        anchorV: Float?,
        animatorIndex: Float?
    ) {
        val builder = LatLngBounds.Builder()

        for (latLng in latLngList) {
            builder.include(latLng)
        }

        val bounds = builder.build()

        if (padding != null) {
            googleMap?.animateCamera(
                CameraUpdateFactory.newLatLngBounds(
                    bounds,
                    padding
                )
            )
        } else {
            googleMap?.animateCamera(
                CameraUpdateFactory.newLatLngBounds(
                    bounds,
                    CAM_PADDING
                )
            )
        }

        val polylineOptions = PolylineOptions()
        polylineOptions.color(Color.GRAY)
        val blackPolylineOptions = PolylineOptions()
        blackPolylineOptions.color(Color.BLACK)

        if (width != null) {
            polylineOptions.width(width)
            blackPolylineOptions.width(width)
        } else {
            polylineOptions.width(CAM_WIDTH)
            blackPolylineOptions.width(CAM_WIDTH)
        }

        polylineOptions.addAll(latLngList)
        greyPolyLine = googleMap?.addPolyline(polylineOptions)

        blackPolyline = googleMap?.addPolyline(blackPolylineOptions)

        if (anchorU != null) {
            if (anchorV != null) {
                originMarker = addOriginDestinationMarkerAndGet(latLngList[0])
                originMarker?.setAnchor(anchorU, anchorV)
                destinationMarker =
                    addOriginDestinationMarkerAndGet(latLngList[latLngList.size - 1])
                destinationMarker?.setAnchor(anchorU, anchorV)
            } else {

                originMarker = addOriginDestinationMarkerAndGet(latLngList[0])
                originMarker?.setAnchor(anchorU, 0f)
                destinationMarker =
                    addOriginDestinationMarkerAndGet(latLngList[latLngList.size - 1])
                destinationMarker?.setAnchor(anchorU, 0f)
            }
        } else {
            originMarker = addOriginDestinationMarkerAndGet(latLngList[0])
            originMarker?.setAnchor(CAM_ANCHOR, CAM_ANCHOR)
            destinationMarker = addOriginDestinationMarkerAndGet(latLngList[latLngList.size - 1])
            destinationMarker?.setAnchor(CAM_ANCHOR, CAM_ANCHOR)
        }

        val polylineAnimator = AnimationUtils.polyLineAnimator()
        polylineAnimator.addUpdateListener { valueAnimator ->
            val percentValue = (valueAnimator.animatedValue as Int)
            val index: Int = if (animatorIndex != null) {
                (greyPolyLine?.points!!.size * (percentValue / animatorIndex)).toInt()
            } else {
                (greyPolyLine?.points!!.size * (percentValue / CAM_INDEX)).toInt()
            }
            blackPolyline?.points = greyPolyLine?.points!!.subList(0, index)
        }
        polylineAnimator.start()
    }

    fun updateCabLocation(context: Context, latLng: LatLng, anchorU: Float?, anchorV: Float?) {
        if (movingCabMarker == null) {
            movingCabMarker = addCarMarkerAndGet(context, latLng)
        }
        if (previousLatLngFromServer == null) {
            currentLatLngFromServer = latLng
            previousLatLngFromServer = currentLatLngFromServer
            movingCabMarker?.position = currentLatLngFromServer

            if (anchorU != null) {
                if (anchorV != null) {
                    movingCabMarker?.setAnchor(anchorU, anchorV)
                } else {
                    movingCabMarker?.setAnchor(anchorU, 0f)
                }
            } else {
                movingCabMarker?.setAnchor(CAM_ANCHOR, CAM_ANCHOR)
            }

            animateCamera(currentLatLngFromServer, ZOOM)
        } else {
            previousLatLngFromServer = currentLatLngFromServer
            currentLatLngFromServer = latLng
            val valueAnimator = AnimationUtils.cabAnimator()
            valueAnimator.addUpdateListener { va ->
                if (currentLatLngFromServer != null && previousLatLngFromServer != null) {
                    val multiplier = va.animatedFraction
                    val nextLocation = LatLng(
                        multiplier * currentLatLngFromServer!!.latitude + (1 - multiplier) *
                                previousLatLngFromServer!!.latitude,
                        multiplier * currentLatLngFromServer!!.longitude + (1 - multiplier) *
                                previousLatLngFromServer!!.longitude
                    )
                    movingCabMarker?.position = nextLocation

                    if (anchorU != null) {
                        if (anchorV != null) {
                            movingCabMarker?.setAnchor(anchorU, anchorV)
                        } else {
                            movingCabMarker?.setAnchor(anchorU, 0.0f)
                        }
                    } else {
                        movingCabMarker?.setAnchor(CAM_ANCHOR, CAM_ANCHOR)
                    }

                    val rotation = MapUtils.getRotation(previousLatLngFromServer!!, nextLocation)
                    if (!rotation.isNaN()) {
                        movingCabMarker?.rotation = rotation
                    }
                    animateCamera(nextLocation, ZOOM)
                }
            }
            valueAnimator.start()
        }
    }

    fun reset(): LatLng? {
        previousLatLngFromServer = null
        currentLatLngFromServer = null
        if (currentLatLng != null) {
            moveCamera(currentLatLng)
            animateCamera(currentLatLng, ZOOM)
        }
        movingCabMarker?.remove()
        greyPolyLine?.remove()
        blackPolyline?.remove()
        originMarker?.remove()
        destinationMarker?.remove()
        greyPolyLine = null
        blackPolyline = null
        originMarker = null
        destinationMarker = null
        movingCabMarker = null

        return currentLatLng
    }
}